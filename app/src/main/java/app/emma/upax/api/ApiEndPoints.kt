package app.emma.upax.api

import app.emma.upax.model.MoviesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiEndPoints {

    @GET("3/movie/popular?")
    fun getMovies(@Query("api_key") api_key: String?) : Call<MoviesResponse>
}