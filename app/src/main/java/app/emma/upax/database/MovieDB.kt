package app.emma.upax.database

import androidx.room.Database
import androidx.room.RoomDatabase
import app.emma.upax.model.MovieDataBase

@Database(entities = [MovieDataBase::class], version = 1)
abstract class MovieDB : RoomDatabase() {
    abstract fun movieDAO(): DAOMovie
}