package app.emma.upax.firebase

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

object FireBaseConnection {

    fun initStorage() : FirebaseStorage{
        return FirebaseStorage.getInstance()
    }

    private fun initFireStore() : FirebaseFirestore{
        return FirebaseFirestore.getInstance()
    }

    fun initCollectionLocations() : CollectionReference{
        return initFireStore().collection("location")
    }

    fun initCollectionImages() : CollectionReference{
        return initFireStore().collection("images")
    }

}