package app.emma.upax.model.data

data class Location(
    val lat : Double,
    val lng : Double
) {
    constructor() : this(
        lat = 0.0,
        lng = 0.0
    )
}