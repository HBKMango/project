package app.emma.upax.utils

import android.app.Activity
import androidx.appcompat.app.AlertDialog

object DialogsUtils {
    fun showDialog(activity: Activity): AlertDialog {
        return activity.let {
            val builder = AlertDialog.Builder(it).setTitle("Necesitas una conexión para continuar")
            builder.apply {
                setPositiveButton("Aceptar") { dialog, id ->
                    activity.finish()
                }
            }
            builder.create()
        }
    }
}