package app.emma.upax.utils

import app.emma.upax.model.Movie
import app.emma.upax.model.MovieDataBase
import app.emma.upax.model.data.ImageOnline
import app.emma.upax.model.data.Location
import com.google.firebase.firestore.DocumentSnapshot
import com.squareup.okhttp.internal.DiskLruCache

object ModelUtils {
    private fun Movie.toMovieDB(): MovieDataBase {
        return MovieDataBase(
            adult = this.adult,
            backdrop_path = this.backdrop_path,
            genre_ids = this.genre_ids.toString(),
            id = this.id,
            original_language = this.original_language,
            original_title = this.original_title,
            overview = this.overview,
            popularity = this.popularity,
            poster_path = this.poster_path,
            release_date = this.release_date,
            title = this.title,
            video = this.video,
            vote_average = this.vote_average,
            vote_count = this.vote_count
        )
    }

    private fun MovieDataBase.toMovie(): Movie {
        return Movie(
            adult = this.adult,
            backdrop_path = this.backdrop_path,
            genre_ids = emptyList(),
            id = this.id,
            original_language = this.original_language,
            original_title = this.original_title,
            overview = this.overview,
            popularity = this.popularity,
            poster_path = this.poster_path,
            release_date = this.release_date,
            title = this.title,
            video = this.video,
            vote_average = this.vote_average,
            vote_count = this.vote_count
        )
    }

    fun toMovieDBList(list: List<Movie>): List<MovieDataBase>{
        val newList = mutableListOf<MovieDataBase>()
        list.forEach {
            newList.add(it.toMovieDB())
        }
        return newList
    }

    fun toMovieList(list: List<MovieDataBase>) :List<Movie>{
        val newList = mutableListOf<Movie>()
        list.forEach {
            newList.add(it.toMovie())
        }
        return newList
    }

    fun DocumentSnapshot.toLocation(): Location? {
        return if (this.exists()) this.toObject(Location::class.java) else
            Location(0.0, 0.0)
    }

    fun DocumentSnapshot.toImage(): ImageOnline?{
        return if (this.exists()) this.toObject(ImageOnline::class.java) else
            ImageOnline(image = "")
    }
}