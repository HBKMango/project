package app.emma.upax.view.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import app.emma.upax.database.MovieDB
import app.emma.upax.databinding.FragmentMoviesBinding
import app.emma.upax.model.Movie
import app.emma.upax.utils.ConnectionUtils
import app.emma.upax.utils.DialogsUtils
import app.emma.upax.utils.ModelUtils
import app.emma.upax.view.adapters.MovieListener
import app.emma.upax.view.adapters.MoviesAdapter
import app.emma.upax.viewmodel.MoviesViewModel
import app.emma.upax.viewmodel.MoviesViewModelFactory
import kotlinx.coroutines.launch

class MoviesFragment : Fragment(), MovieListener {

    private lateinit var binding: FragmentMoviesBinding
    private lateinit var adapter: MoviesAdapter
    private lateinit var load : LoadFragment
    private lateinit var room : MovieDB

    private val viewModel: MoviesViewModel by viewModels() {
        MoviesViewModelFactory(requireContext())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        load = LoadFragment()
        adapter = MoviesAdapter()
        room = Room
            .databaseBuilder(context, MovieDB::class.java, "Movie")
            .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        load.show(childFragmentManager, "Load")
        if (ConnectionUtils.isOnline(requireContext())){
            viewModel.getMovies()
        }else{
            viewModel.getMoviesFromDB(requireContext())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.listener = this
        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter

        val manager = GridLayoutManager(activity, 3, GridLayoutManager.VERTICAL, false)
        binding.recycler.layoutManager = manager

        viewModel.movies.observe(viewLifecycleOwner, { movies ->
            load.dismiss()
            if (movies.isEmpty()) {
                DialogsUtils.showDialog(requireActivity()).show()
            } else{
                adapter.swapData(movies)
                saveInDataBase(movies)
            }
        })

        viewModel.exception.observe(viewLifecycleOwner, { exception ->
            Toast.makeText(requireContext(), exception, Toast.LENGTH_LONG).show()
            load.dismiss()
        })
    }

    override fun onMovieSelected(movie: Movie) {
        MoviePreviewFragment(movie).show(childFragmentManager, "Movie Preview")
    }

    private fun saveInDataBase(movies: List<Movie>) {
        lifecycleScope.launch {
            if (room.movieDAO().getMovies().isEmpty()) {
                room.movieDAO().addMovie(ModelUtils.toMovieDBList(movies))
            }else{
                Log.d("DB", "Base poblada")
            }
        }
    }
}