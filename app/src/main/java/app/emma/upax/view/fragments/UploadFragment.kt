package app.emma.upax.view.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.emma.upax.databinding.FragmentUploadBinding
import app.emma.upax.view.adapters.ImagesAdapter
import app.emma.upax.viewmodel.UploadViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import java.io.File

class UploadFragment : Fragment() {

    lateinit var binding: FragmentUploadBinding
    private lateinit var adapter: ImagesAdapter
    private lateinit var load : LoadFragment
    private val viewModel : UploadViewModel by viewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        adapter = ImagesAdapter()
        load = LoadFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getImages()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUploadBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        binding.recycler.adapter = adapter
        val manager = GridLayoutManager(activity, 3, GridLayoutManager.VERTICAL, false)
        binding.recycler.layoutManager = manager

        binding.add.setOnClickListener {
            selectImage()
        }

        viewModel.images.observe(viewLifecycleOwner, {
            adapter.swapData(it)
        })

        viewModel.upload.observe(viewLifecycleOwner, {
            load.dismiss()
        })

        viewModel.errorImages.observe(viewLifecycleOwner, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        viewModel.errorUpload.observe(viewLifecycleOwner, {
            load.dismiss()
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        /*var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val data: Intent? = result.data
            when (result.resultCode) {
                Activity.RESULT_OK -> {
                    val fileUri = data?.data
                    Toast.makeText(requireContext(), fileUri.toString(), Toast.LENGTH_SHORT).show()
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                }
            }
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            Activity.RESULT_OK -> {
                load
                val fileUri = data?.data.toString().replace("file://", "")
                //val uri = Uri.fromFile(File(fileUri))
                val uri = Uri.parse(fileUri)
                Log.i("LOGURI", uri.toString())
                load.show(childFragmentManager, "Load")
                viewModel.uploadImage(uri)
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun selectImage() {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024)			//Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }
}