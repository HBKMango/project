package app.emma.upax.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.emma.upax.model.ModelMap
import app.emma.upax.model.data.Location

class MapViewModel : ViewModel() {

    val locations = MutableLiveData<List<Location>>()
    val error = MutableLiveData<String>()

    val model = ModelMap(this)

    fun getLocations(){
        model.getLocations()
    }
}